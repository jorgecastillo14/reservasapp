import { Injectable } from "@angular/core";
import { Place } from "../places/place.model";

@Injectable({
  providedIn: "root"
})
export class PlacesService {
  private places: Place[] = [
    {
      id: '1',
      title: "Casa",
      imageURL:
        "/assets/Casa/casa.jpg",
      imageURL2:
      "/assets/Casa/casa.jpg",
      imageURL3:
      "/assets/Casa/casa.jpg",
      dato: "Casa ",
      anfitrion: "Jorge",
      price: "200 000",
      descripcion: "6 Personas", 
      comments: ["Casa entera, incluye todos los servicios y totalmente equipada "]
    },
    {
      id: '2',
      title: "Casa playa",
      imageURL:
        "/assets/CasaPlaya/casa.jpg",
      imageURL2:
      "/assets/CasaPlaya/casa.jpg",
      imageURL3:
      "/assets/CasaPlaya/casa.jpg",
      dato: "",
      anfitrion: "Luis",
      price: "55 000", 
      descripcion: "2 Personas",
      comments: ["Casa de playa entera, incluye todos los servicios y totalmente equipada"]
    },
    {
      id: '3',
      title: "Cuarto",
      imageURL:
        "/assets/Cuarto/cuarto.jpg",
      imageURL2:
      "/assets/Cuarto/cuarto.jpg",
      imageURL3:
      "/assets/Cuarto/cuarto.jpg",
      dato: "",
      anfitrion: "Jorge",
      price: "15 000", 
      descripcion: "2 Personas",
      comments: ["Cuarto dentro de casa, incluye todos los servicios y totalmente equipada"]
    },
    {
      id: '4',
      title: "Hotel",
      imageURL:
        "/assets/Hotel/hotel.jpg",
      imageURL2:
      "/assets/Hotel/hotel.jpg",
      imageURL3:
      "/assets/Hotel/hotel.jpg",
      dato: "",
      anfitrion: "Luis Ca",
      price: "150 000",
      descripcion: "3 Personas",
      comments: ["Hotel cuarto todo incluido, incluye todos los servicios y totalmente equipada"]
    },
    {
      id: '5',
      title: "Bosque Tienda",
      imageURL:
        "/assets/Tienda/campar.jpg",
      imageURL2:
      "/assets/Tienda/campar.jpg",
      imageURL3:
      "/assets/Tienda/campar.jpg",
      dato: "",
      anfitrion: "Jorge",
      price: "5000",
      descripcion: "2 huéspedes  1 cama 1 baño", 
      comments: ["Tienda de acampar "]
    },
  ];

  constructor() {}

  getPlaces(): Place[] {
    return [...this.places];
  }

  getPlace(placeId: string) {
    return {
      ...this.places.find(place => {
        return place.id === placeId;
      })
    };
  }

  addPlace(title, imageURL, imageURL2, imageURL3, price, dato, anfitrion, descripcion) {
    this.places.push({
      title,
      imageURL,
      imageURL2,
      imageURL3,
      dato,
      anfitrion,
      price,
      descripcion,
      comments: [],
      id: this.places.length + 1 + ""
    });
  }

deletePlace(placeId: string) {
    this.places = this.places.filter(place => {
      return place.id !== placeId;
    });
  }

  getPlacesSearch(): Place[] {
    return [...this.places];
  }
}